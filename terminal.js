/*
babel src/app.js --out-file=public/scripts/app.js --presets=env,react
babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch

babel src/playground/es6-let-const.js --out-file=public/scripts/app.js --presets=env,react --watch
babel src/playground/es6-arrow-function.js --out-file=public/scripts/app.js --presets=env,react --watch
babel src/playground/es6-arrow-function-2.js --out-file=public/scripts/app.js --presets=env,react --watch
babel src/playground/build-it-visible.js --out-file=public/scripts/app.js --presets=env,react --watch
babel src/playground/es6-classes-1.js --out-file=public/scripts/app.js --presets=env,react --watch
babel src/playground/counter-example.js --out-file=public/scripts/app.js --presets=env,react --watch
babel src/playground/jsx-indecision.js --out-file=public/scripts/app.js --presets=env,react --watch

live-server public

yarn install

public/index.html
<script src="https://unpkg.com/react@15/dist/react.js"></script>
<script src="https://unpkg.com/react@16.0.0/umd/react.development.js"></script>
<script src="https://unpkg.com/react@16.6.0/umd/react.development.js"></script>                   // Newest vision

<script src="https://unpkg.com/react-dom@15/dist/react-dom.js"></script>
<script src="https://unpkg.com/react-dom@16.0.0/umd/react-dom.development.js"></script>
<script src="https://unpkg.com/react-dom@16.6.0/umd/react-dom.development.js"></script>           // Newest vision


yarn run serve
yarn run build
yarn run build-babel
 */