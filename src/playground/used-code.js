// JSX - JavaScript XML
/*
if statements
ternary operators
logical and operator
 */

/*
const user = {
  name: 'Jared Berry',
  age: 29,
  location: 'Cape Town'
};
*/

/*
var userName = 'Andrew Mead';
var userAge = 26;
var userLocation = 'Philadelphia';
*/
/*
function getLocation(location) {
  if (location) {
    return <p>Location: {location}</p>;
  }
}

const templateTwo = (
  <div>
    <h1>{user.name ? user.name : 'Anonymous'}</h1>
    {(user.age && user.age >= 21) && <p>Age: {user.age}</p>}
    {getLocation(user.location)}
  </div>
);
 */

// const templateTwo = (
//     <div>
//         <h1>Count: {count}</h1>
//         <button onClick={() => {
//             console.log('some value here')
//         }}>+1</button>
//     </div>
// );

// const obj = {
//   name: 'Jared',
//   getName() {
//     return this.name;
//   }
// };
//
// const getName = obj.getName.bind(obj);
//
// console.log(obj.getName());

//import'./utils';
// import subtract, { square, add } from './utils';
//
// console.log('app.js is running');
// console.log(square(4));
// console.log(add(100, 23));
// console.log(subtract(100, 20));


// import isSenior, { isAdult, canDrink } from './person.js';
// console.log(isAdult(17));
// console.log(canDrink(19));
// console.log(isSenior(64));

// import ReactDOM from 'react-dom';

// const template = <p>THIS IS JSX FROM WEBPACK</p>;
// ReactDOM.render(template, document.getElementById('app'));

// class OldSyntax {
//   constructor() {
//     this.name = 'Mike';
//     this.getGreeting = this.getGreeting.bind(this);
//   }
//   getGreeting() {
//     return `Hi. My name is ${this.name}.`;
//   }
// }
// const oldSyntax = new OldSyntax();
// const getGreeting = oldSyntax.getGreeting;
// console.log(getGreeting());
//
// // --------------------------------
//
// class NewSyntax {
//   name = 'Jen';
//   getGreeting = () => {
//     return `Hi. My name is ${this.name}.`;
//   }
// }
// const newSyntax = new NewSyntax();
// const newGetGreeting = newSyntax.getGreeting
// console.log(newGetGreeting());
