/*const square = function (x) {
  return x * x;
};
 */
function square(x) {
  return x * x;
};

console.log(square(3));

/*
const squareArrow = (x) => {
  return x * x;
};
 */

const squareArrow = (x) => x * x;    // same as above one.

console.log(squareArrow(4))

// const getFirstName = (fullName) => {
//   return fullName.split(' ')[0];
// };
//
// console.log(getFirstName('Mike Smith'));

const getFirstName = (fullName) => fullName.split(' ')[0];

console.log(getFirstName('Mike Smith'));