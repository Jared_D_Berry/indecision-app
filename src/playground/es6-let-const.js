var nameVar = 'Jared';
var nameVar = 'Andrew';
console.log('nameVar', nameVar);

let nameLet = 'Jen';
nameLet = 'Julie';            // Can re assign
// let nameLet = 'Julie';     // can not redefine \ Duplicate declaration
console.log('nameLet', nameLet);

const nameConst = 'Frank';
//nameConst = 'Gunther';            // Can not re assign
// const nameConst = 'Gunther';     // can not redefine \ Duplicate declaration
console.log('nameConst', nameConst);

/*
function getPetName() {
  //var petName = 'Hal';
  //let petName = 'Hal';
  const petName = 'Hal';
  return petName;
}

//getPetName();                       //petName is not defined
const petName = getPetName();
console.log(petName);
 */

// Block scoping

//var fullName = 'Andrew Mead';
//let firstName;
const fullName = 'Andrew Mead';
//const firstName;
let firstName;

if (fullName) {
  //var firstName = fullName.split(' ')[0];
  //const firstName = fullName.split(' ')[0];
  //let firstName = fullName.split(' ')[0];
  firstName = fullName.split(' ')[0];
  console.log(firstName);
}

console.log(firstName);