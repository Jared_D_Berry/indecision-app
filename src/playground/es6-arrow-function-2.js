// arguments object - no longer bond with arrow functions

//const add = function (a, b) {                             // ES5 Functions
const add = (a, b) => {
//    console.log(arguments);                               //Uncaught ReferenceError: arguments is not defined
    return a + b;
};
console.log(add(55, 1), 1001);

// this keyword - no longer bond

const user = {
  name: 'Jared',
  cities: ['Cape Town', 'Johannesburg'],
  //printPlacesLived: function () {                                                              // ES5 functions
  printPlacesLived () {                                                                          // ES6 methods defintions syntax
    //  const cityMessages = this.cities.map((city) => {
      return this.cities.map((city) => this.name + ' has lived in ' + city);               //{
       // return city;
        //return city + '!';
       // return this.name + ' has lived in ' + city;
     // });
    //  return cityMessages


  //printPlacesLived: () => {                                                                      // Uncaught TypeError: Cannot read property 'cities' of undefined, this => dose not bond, no longer = to object.
      //console.log(this.name);
      //console.log(this.cities);
      //const that = this                                                                           // Work around

     // this.cities.forEach(function (city) {
     //  this.cities.forEach((city) => {
     //      console.log(this.name + ' has lived in ' + city);
     //  })
  }
};

//user.printPlacesLived();
console.log(user.printPlacesLived());

// Challenge

const multiplier = {
    numbers: [1, 2, 3],
    multiplyBy: 3,
    multiply() {
        return this.numbers.map((number) => number * this.multiplyBy);
    }
};

console.log(multiplier.multiply());